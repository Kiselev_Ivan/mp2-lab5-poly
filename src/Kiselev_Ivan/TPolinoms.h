//������ ������ ���������
#ifndef _TPolinoms_
#define _TPolinoms_


#include "THeadRing.h"
#include "TMonom.h"


typedef TMonom* PTMonom;

class TPolinom : public THeadRing {
public:
	TPolinom(int monoms[][2] = NULL, int km = 0);
	 //km => ��������� �������, �� � ������
	 // �����������
	 // �������� �� ������� ������������-������

	TPolinom (TPolinom &q);     // ����������� �����������
	~TPolinom() {};
	PTMonom  GetMonom() { return (PTMonom)GetDatValue(); }

	TPolinom& operator+(TPolinom &q); // �������� ���������
	TPolinom& operator=(TPolinom &q); // ������������
	bool operator==(TPolinom &q);				   //�������������� �������

	friend ostream& operator<<(ostream &os, TPolinom &q);
	void AddMonom(TMonom* monom);
};

TPolinom::TPolinom(int monoms[][2], int km)
{
	
	PTMonom Monom = new TMonom(0, 0);
	pHead->SetDatValue(Monom);
	for (int i = 0; i < km; i++)
	{
		Monom = new TMonom(monoms[i][0], monoms[i][1]);
		InsLast(Monom);
	}
	Reset();
}

TPolinom::TPolinom(TPolinom & q)
{
		PTMonom Monom = new TMonom(0, 0);
		pHead->SetDatValue(Monom);
		for (q.Reset();!q.IsListEnded(); q.GoNext())
		{
			InsLast(q.GetDatValue());
		}
		pHead->SetNextLink(pFirst);
		Reset();
		q.Reset();
}

TPolinom & TPolinom::operator+(TPolinom & q)
{
	TPolinom* old = new TPolinom(q);
	Reset();
	while (!IsListEnded())
	{
		q.Reset();
		old->AddMonom(GetMonom());
		GoNext();
	}
	Reset();
	q.Reset();
	return *old;
}

TPolinom & TPolinom::operator=(TPolinom & q)
{
	for (q.Reset(); !q.IsListEnded(); q.GoNext())
	{
		InsLast(q.GetDatValue());
	}
	pHead->SetNextLink(pFirst);
	Reset();
	q.Reset();
	return *this;
}

bool TPolinom::operator==( TPolinom &q)
{
	if (pFirst == q.pFirst) return true;
	if (this->ListLen != q.ListLen)
		return false;
	else
	{
		PTMonom pMon, qMon;
		Reset(); 
		q.Reset();
		while (!IsListEnded())
		{
			pMon = GetMonom();				
			qMon = q.GetMonom();			
			if (*pMon == *qMon)
			{
				GoNext(); 
				q.GoNext();
			}
			else
				return false;
		}
		return true;
	}
}

// ������������
void TPolinom::AddMonom(TMonom * monom)
{
	Reset();
	while (!IsListEnded() && (GetMonom()->GetIndex() > monom->GetIndex()))
	{
		GoNext();
	}
	if (!IsListEnded())
		if (GetMonom()->GetIndex() == monom->GetIndex())
		{
			GetMonom()->SetCoeff(monom->GetCoeff() + GetMonom()->GetCoeff());
			if (GetMonom()->GetCoeff() == 0) 
				DelCurrent();
		}
		else
		{
			InsCurrent(monom->GetCopy());
		}
	else InsLast(monom->GetCopy());
}

ostream& operator<<(ostream &os, TPolinom &q)
{
	TMonom* old = new TMonom();
	for (q.Reset(); !q.IsListEnded(); q.GoNext())
	{
		old = q.GetMonom();
		cout << old;
	}

	return os;
}

#endif

