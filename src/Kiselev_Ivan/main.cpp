#include <iostream>
#include "../TPolinoms.h"
using namespace std;

int main()
{
	const int size = 3;
	int mon1[][2] = { { 2, 100 } ,{ 2, 4 }, { 1, 3 } };
	int mon2[][2] = { { 2, 100 } ,{ -2, 4 },{ 3, 3 } };
	TPolinom Pol1(mon1, size);
	TPolinom Pol2(mon2, size);
	cout << endl << Pol1 << endl << Pol2 << endl;
	TPolinom ResPol(Pol1 + Pol2);
	cout << ResPol << endl;
	system("pause");
	return 0;
}